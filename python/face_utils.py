import numpy
import mediapipe
import cv2


def get_landmark_array(image: numpy.ndarray) -> numpy.ndarray:
    """
    Arrange mediapipe face landmarks in numpy array
    :param image: RGB uint8 image of face
    :return: array of size Nx2 containing N landmarks coordinates (x,y)
    """
    landmarks_array = list()
    with mediapipe.solutions.face_mesh.FaceMesh(
            static_image_mode=True,
            max_num_faces=1,
            refine_landmarks=True,
            min_detection_confidence=0.5) as face_mesh:
        landmarks = face_mesh.process(image)
        for face_landmarks in landmarks.multi_face_landmarks:
            for kp in face_landmarks.landmark:
                landmarks_array.append([image.shape[1] * kp.x, image.shape[0] * kp.y, 1])
            return numpy.array(landmarks_array)


def align_faces_landmarks(ref_im: numpy.ndarray, im_to_align: numpy.ndarray) -> numpy.ndarray:
    """
    Align a face image wrt ref_im and return the aligned (warped) image. Face landmarks are used as keypoints for
    alignment and partial 2D affine transform is estimated using RANSAC.
    :param ref_im: Reference image for alignment
    :param im_to_align:
    :return:
    """
    l1 = get_landmark_array(ref_im)
    l2 = get_landmark_array(im_to_align)
    affine_mat = cv2.estimateAffinePartial2D(l2[:, :2].astype('float32'),
                                             l1[:, :2].astype('float32'),
                                             method=cv2.RANSAC)[0]
    return cv2.warpAffine(im_to_align, affine_mat, im_to_align.shape[:2][::-1])
